//data, jqXHR, textStatus, errorThrown
function userlogin(event, loginform) {
  event.preventDefault();

  $('#login').fadeOut();
  $.ajax({
        type: 'POST',
        url: '/ajax/login.php',
        data: $(loginform).serializeArray(),
        dataType: 'json',
        success: function(data) {
          if (data.status == 'error') {
            alert(data.statusmsg);
            $('#login').fadeIn();
          }
          if (data.status === 'success') {
            window.location = '/account/';
          }
        },
        error: LogonError
  });
}

function LogonError() {
  alert('Error: The system could not log you in.' +
      '\n\nIf you believe this is an error please email the' +
      '\nAdministrator at admin@blacklistlogistics.com');
}


function reguser(event, regform) {
  event.preventDefault();

  $('#register').fadeOut();

  $.ajax({
    type: 'POST',
    url: '/ajax/register.php',
    data: $(regform).serializeArray(),
    dataType: 'json',
    success: function(data) {
      if (data.status == 'success') {
        $('#regmsg').addClass('alert alert-success');
        $('#regmsg').html(data.statusmsg);
      }
      if (data.status == 'error') {
        $('#regmsg').addClass('alert alert-danger');
        $('#regmsg').html(data.statusmsg);
        $('#register').fadeIn();
      }
    },
    error: function() {
      $('#regmsg').addClass('alert alert-danger');
      $('#regmsg').html('There was an error. Please ' +
          'inform the site administrator.');
    }
  });
}

function logout(event) {
	event.preventDefault();

	$.ajax({
		url: '/ajax/logout.php',
		success: function() {
			window.location = '/'
		}
	});
}

function addapikey(event, apiform) {
	event.preventDefault();

	$('#submitapi').fadeOut();
  $('#apimsg').fadeOut();
  $('#apimsg').html($('#apimsg').text());
  $('#apimsg').removeClass('alert alert-danger alert-success');

	$.ajax({
		type: 'POST',
		url: '/ajax/addapi.php',
		data: $(apiform).serializeArray(),
		dataType: 'json',
		success: function(data) {
			if (data.status == 'success') {
				$('#apimsg').addClass('alert alert-success');
				$('#apimsg').html(data.msg);
        $('#apimsg').fadeIn();
				$('#submitapi').fadeIn();
				getpilotkeys();
			}
			if (data.status == 'error') {
				$('#submitapi').fadeIn();
				$('#apimsg').addClass('alert alert-danger');
				$('#apimsg').html(data.msg);
        $('#apimsg').fadeIn();
				$('#submitapi').fadeIn();
			}
		},
		error: function(data, jqXHR, textStatus, errorThrown) {
			alert(data + jqXHR + textStatus + errorThrown);
			$('#apimsg').addClass('alert alert-danger text-center center-block');
			$('#apimsg').html('There was an error. Please ' +
			'inform the site administrator.');
		}
	});


}

function getpilotkeys() {


	$.ajax({
		url: '/ajax/getapikeys.php',
		dataType: 'json',
		success: function(data) {
			if (data.status == 'success') {
				$('#keys').html(data.msg);
        $('#keys').fadeIn();
			}
			if (data.status == 'error') {
				$('#keys').html(data.msg);
        $('#keys').fadeIn();
			}
		}
	});
}

function deletepilotkeys(event, keyid) {
  event.preventDefault();

  $('#keys').fadeOut();

  $.ajax({
    type: 'POST',
    url: '/ajax/deletekeys.php',
    data: {keyid: keyid},
    dataType: 'json',
    success: function(data) {
      if (data.status == 'success') {
        getpilotkeys();
      }
    }
  });
}

function changepass(event, passform) {
    event.preventDefault();

    $('#submitpass').fadeOut();
    $('#passmsg').fadeOut();
    $('#passmsg').html($('#apimsg').text());
    $('#passmsg').removeClass('alert alert-danger alert-success');

    $.ajax({
        type: 'POST',
        url: '/ajax/changepass.php',
        data: $(passform).serializeArray(),
        dataType: 'json',
        success: function(data) {
            if (data.status == 'success') {
                $('#passmsg').addClass('alert alert-success');
                $('#passmsg').html(data.msg);
                $('#passmsg').fadeIn();
                $('#submitpass').fadeIn();
            }
            if (data.status == 'error') {
                $('#submitpass').fadeIn();
                $('#passmsg').addClass('alert alert-danger');
                $('#passmsg').html(data.msg);
                $('#passmsg').fadeIn();
                $('#submitpass').fadeIn();
            }
        },
        error: function(data, jqXHR, textStatus, errorThrown) {
            alert(data + jqXHR + textStatus + errorThrown);
            $('#apimsg').addClass('alert alert-danger text-center center-block');
            $('#apimsg').html('There was an error. Please ' +
                'inform the site administrator.');
        }
    });


}

function changeemail(event, emailform) {
    event.preventDefault();

    $('#submitemail').fadeOut();
    $('#emailmsg').fadeOut();
    $('#emailmsg').html($('#emailmsg').text());
    $('#emailmsg').removeClass('alert alert-danger alert-success');

    $.ajax({
        type: 'POST',
        url: '/ajax/changeemail.php',
        data: $(emailform).serializeArray(),
        dataType: 'json',
        success: function(data) {
            if (data.status == 'success') {
                $('#emailmsg').addClass('alert alert-success');
                $('#emailmsg').html(data.msg);
                $('#emailmsg').fadeIn();
                $('#submitapi').fadeIn();
            }
            if (data.status == 'error') {
                $('#submitemail').fadeIn();
                $('#apimsg').addClass('alert alert-danger');
                $('#apimsg').html(data.msg);
                $('#apimsg').fadeIn();
                $('#submitapi').fadeIn();
            }
        },
        error: function(data, jqXHR, textStatus, errorThrown) {
            alert(data + jqXHR + textStatus + errorThrown);
            $('#emailmsg').addClass('alert alert-danger text-center center-block');
            $('#emailmsg').html('There was an error. Please ' +
                'inform the site administrator.');
        }
    });


}