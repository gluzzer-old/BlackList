<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.inc';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/checklogin.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/sessiontimer.php';
?>
<div class="container-fluid">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="well">
            <div class="row">
                <h3 class="center-block text-center">Jobs In Progress</h3>
                <div class="text-center" id="inprogress">
                    INPROGRESS
                </div>
            </div>
            <br />
            <div class="row">
                <h3 class="center-block text-center">Jobs Completed</h3>
                <div class="text-center" id="completed">
                    COMPLETED
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="center-block text-center">
                    <button class="btn btn-default btn-primary">New Job</button>
                </div>
            </div>
        </div>
    </div>
</div>
