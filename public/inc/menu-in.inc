<div class="container-fluid">
    <div class="col-lg-1"></div>
    <nav role="navbar-header" class="navbar navbar-inverse col-lg-10">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/account/" class="navbar-brand">BL Home</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Public Tools <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="dropdown-header">Public Tools</li>
                        <li><a href="#">Comming Soon!</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Assets<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Corp Assets</a></li>
                        <li><a href="#">Player Assets</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/tools/jobs.php">Job Tracker</a></li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-inverse navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <span class="caret"></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="dropdown-header">Account Management Tools</li>
                            <li><a href="/account/apimanagement.php">Manage API Keys</a></li>
                            <li><a href="/account/accountmanagement.php">Manage Account</a></li>
                            <li><a href="#">More Comming Soon!</a></li>
                            <li class="divider"></li>
                            <li><a href="#" data-toggle="dropdown" id="navLogin" onclick="logout(event)">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="col-lg-1"></div>
</div>