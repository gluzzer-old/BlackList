<?php
if (!isset($_SESSION['login']) || $_SESSION['login'] != 1) {
    header('Location: https://' . $_SERVER['HTTP_HOST'] . '/');
}