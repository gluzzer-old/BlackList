<?php
session_start();
require_once '../../private/settings.inc';
require_once '../../private/functions.php';

logout();

$response = array();
$response['status'] = 'success';
echo json_encode($response);
return;