<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\Members\Access;

$now = time();
if(isset($_SESSION['sessiontimer']) && $now > $_SESSION['sessiontimer']) {
    Access::logout();
    header('Location: ' . $_SERVER['HTTP_HOST']);
}
$_SESSION['sessiontimer'] = $now + 1200;