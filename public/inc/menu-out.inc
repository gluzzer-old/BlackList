<div class="container-fluid">
    <div class="col-lg-1"></div>
    <nav role="navbar-header" class="navbar navbar-inverse col-lg-10">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand">BL Home</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Public Tools <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="dropdown-header">Public Tools</li>
                        <li><a href="#">Comming Soon!</a></li>
                    </ul>
                </li>
                <!-- Place More Links Here -->
            </ul>
            <div>
                <ul class="nav navbar-nav navbar-inverse navbar-right">
                    <li class="dropdown" id="loginmenu">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                        <div class="dropdown-menu col-lg-3" style="padding: 17px; background-color: #222222">
                            <form id="formLogin">
                                <input class="form-control center-block text-center" name="username" id="username" placeholder="Username" type="text" required>
                                <input class="form-control center-block text-center" name="password" id="password" placeholder="Password" type="password" required>
                                <input type="button" value="Login" id="login" class="btn btn-block btn-primary center-block" onclick="userlogin(event, '#formLogin')">
                            </form>
                            <br />
                            <a href="/register.php" class="text-center center-block">Register Here!</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="col-lg-1"></div>
</div>