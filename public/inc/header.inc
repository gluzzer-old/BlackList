<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blacklist Logistics</title>
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/custom.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
    <div>
        <img src="/images/bl-logo.png" class="center-block img-responsive">
    </div>
</div>
<?php
if(isset($_SESSION['login']) && $_SESSION['login'] === 1) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/menu-in.inc';
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/menu-out.inc';
}
