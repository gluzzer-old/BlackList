<div class="well hidden"></div>
<div class="container">
    <div class="well col-lg-10 col-lg-offset-1 text-center">
        <a href="https://blacklistlogistics.com">&copy;Blacklist Logistics</a>

        <?php
        $start_date = 2015;
        if ($start_date != date('Y')) {
            echo $start_date . ' - ' . date('Y') . '.';
        } else {
            echo $start_date . '.';
        }
        ?>

        All rights reserved.
        <br />
        <a href="http://www.ccpgames.com">©CCP</a> hf. All rights reserved. Used with permission.
        <br />
        <br />
        <a href="/terms.php">Terms, Conditions, and Privacy</a>
    </div>
</div>