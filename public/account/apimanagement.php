<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.inc';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/checklogin.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/sessiontimer.php';
?>
<div class="row col-lg-10 col-lg-offset-1">
   <div class="well" id="apiadd">
       <h3 class="text-center">Add a new API key</h3>
       <br />
       <div id="apimsg" class="text-center center-block"></div>
       <form id="addapiform" class="form-horizontal col-lg-offset-3">
           <div class="form-group">
               <label for="keyid" class="col-md-2 control-label">Key ID</label>
               <div class="col-md-2">
                   <input type="text" class="form-control" id="keyID" name="keyID" placeholder="Key ID">
               </div>
           </div>
           <div class="form-group">
               <label for="vcode" class="col-md-2 control-label">Verification Code</label>
               <div class="col-md-6">
                   <input type="text" class="form-control" id="=vCode" name="vCode" placeholder="Verification Code">
               </div>
           </div>
           <div class="form-group">
               <div class="col-md-offset-2 col-md-10">
                   <button type="submit" class="btn btn-primary" id="submitapi" name="submitapi" onclick="addapikey(event, '#addapiform')">Submit</button>
               </div>
           </div>
       </form>
   </div>
</div>
<div class="row col-lg-10 col-lg-offset-1">
    <div class="well well-lg">
        <h3 class="text-center">Current API Keys</h3>
        <div class="col-lg-offset-3" id="keys"></div>
    </div>
</div>
<script type="application/javascript">
    $(document).ready(getpilotkeys());
</script>