<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.inc';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/checklogin.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/sessiontimer.php';
?>
<div class="row col-lg-10 col-lg-offset-1">
    <div class="well" id="changepassword">
        <h3 class="text-center">Change Password</h3>
        <br />
        <div id="passmsg" class="text-center center-block"></div>
        <form id="passform" class="form-horizontal col-lg-offset-3">
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <input type="password" class="form-control" id="newpass" name="newpass" placeholder="New password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <input type="password" class="form-control" id="newpassval" name="newpassval" placeholder="Re-enter new password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <button type="submit" class="btn btn-primary center-block text-center" id="submitpass" name="submitpass" onclick="changepass(event, '#passform')">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<br />
<div class="row col-lg-10 col-lg-offset-1">
    <div class="well" id="changepassword">
        <h3 class="text-center">Change Email</h3>
        <br />
        <div id="emailmsg" class="text-center center-block"></div>
        <form id="emailform" class="form-horizontal col-lg-offset-3">
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <input type="email" class="form-control" id="newemail" name="newemail" placeholder="New email">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <input type="email" class="form-control" id="newemailval" name="newemailval" placeholder="Re-enter new email">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <button type="submit" class="btn btn-primary center-block text-center" id="submitemail" name="submitemail" onclick="changeemail(event, '#emailform')">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>