<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.inc';
?>
<div class="container well text-center">
    <h2 class="text-center">Terms, Conditions, and Usage</h2>
    <h3>What this Covers</h3>
    <br />
    <h4>Blacklist Logistics takes privacy seriously.</h4>
    <p>
        Blacklist Logistics will encrypt all data transmission between your browser and our servers.<br />
        We will also not store any passwords in plain text or any type of encryption that is not suitable for the <br />
        purpose of storing passwords.
        <br />
        <br />
        We will also never sell, give away, or otherwise release any type of personal information<br />
        (email, username, API keys, etc) without your express written permission or the proper legal <br />
        process.
    </p>
    <br />
    <br />
    <h4>Data Usage</h4>
    <p>
        By using this site you agree to allow Blacklist Logistics to analyze any captured data as seen fit for<br />
        perfermance, maintince, statistics, etc.<br />
        No data will be collected in a format that can be used to identify a users characters, in-game activities, etc.<br />
    </p>
    <br />
    <br />
    <h4>Usage and Liability</h4>
    <p>
        By using this site you agree that Blacklist Logistics can use any information provided by you <br />
        (username, email, API) as governed above.
        <br />
        <br />
        You also acknowledge that this site is provided as is as stated below:<br />
        <p class="well col-md-offset-1 col-md-10">THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING<br />
        BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. <br />
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, <br />
        WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE <br />
        OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>


</div>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.inc';