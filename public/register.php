<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.inc';
?>
<div class="container-fluid">
    <div class="col-lg-4"></div>
    <div class="well col-lg-4 text-center">
        <form name="regform" id="regform">
            <div id="regmsg"></div>
            <h1>Please Register Below</h1>
            <input type="text" class="form-control center-block text-center"
                   name="username" id="username" placeholder="Username" required autofocus=""
                   style="max-width: 360px; margin-bottom: -1px;"/>
            <input type="password" class="form-control center-block text-center"
                   name="password" id="password" placeholder="Password" required
                   style="max-width: 360px; margin-bottom: -1px;"/>
            <input type="email" class="form-control center-block text-center"
                   name="email" id="email" placeholder="Email" required=""
                   style="max-width: 360px; margin-bottom: -1px;"/>
            <input class="btn btn-lg btn-primary center-block"
                    type="button" name="register" id="register" style="max-width: 360px;
                    margin-bottom: 10px; margin-top: 10px;" value="Register"
                    onclick="reguser(event, '#regform')">
        </form>
    </div>
    <div class="alert-info" id="info">

    </div>
</div>
