<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\API\Manage;

$keyid = @$_POST['keyid'];

$response = array();

$getkeys = new Manage;
$keys = $getkeys->deletepilotkeys($_SESSION['MemberID'], $keyid);
sleep(1);

if($keys === 0) {
    $response['status'] = 'error';
    $response['msg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
}
if($keys === 1) {
    $response['status'] = 'success';
    echo json_encode($response);
    return;
}