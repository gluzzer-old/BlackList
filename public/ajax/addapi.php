<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\API\Manage;

$keyID = @$_POST['keyID'];
$vCode = @$_POST['vCode'];

$response = [];
$errors = 0;

if($keyID == null) {
    $errors = 1;
    $response['msg'] .= 'Please enter your KeyID<br />';
}
if($vCode == null) {
    $errors = 1;
    $response['msg'] .= 'Please enter your Verification Code<br />';
}
if($errors === 1) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
} else {
    $api = new Manage;
    $emailstatus = $api->addApi($_SESSION['UserID'], $keyID, $vCode);

    if($emailstatus === 0) {
        $response['status'] = 'error';
        $response['msg'] = 'Error: Key is either invalid or expired';
        echo json_encode($response);
        return;
    }
    if($emailstatus === 2) {
        $response['status'] = 'error';
        $response['msg'] = 'Error: The system was unable to process your request. Please try again later.';
        echo json_encode($response);
        return;
    }
    if($emailstatus === 1) {
        $response['status'] = 'success';
        $response['msg'] = 'Your Key was added successfully.';
        echo json_encode($response);
        return;
    }

    $response['status'] = 'error';
    $response['msg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
}