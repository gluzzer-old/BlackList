<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\Members\Change;

$email = @$_POST['newemail'];
$emailval = @$_POST['newemailval'];

$response = [];
$errors = 0;

if($email == null) {
    $errors = 1;
    $response['msg'] .= 'Please enter your new email address<br />';
}
if($emailval == null) {
    $errors = 1;
    $response['msg'] .= 'Please re-enter your new email address<br />';
}
if($errors === 1) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
} else {
    $emailchange = new Change;
    $emailstatus = $emailchange->changeEmail($_SESSION['UserID'], $email);

    if($emailstatus === 0) {
        $response['status'] = 'error';
        $response['msg'] = 'Error: The system was unable to process your request. Please try again later.';
        echo json_encode($response);
        return;
    }
    if($emailstatus === 1) {
        $response['status'] = 'success';
        $response['msg'] = 'Your email was successfully updated.';
        echo json_encode($response);
        return;
    }

    $response['status'] = 'error';
    $response['msg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
}