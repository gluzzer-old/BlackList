<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\Members\Access;

$logout = Access::logout();
if ($logout === 1) {
    $response = array();
    $response['status'] = 'success';
    echo json_encode($response);
    return;
}