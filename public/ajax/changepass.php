<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\Members\Change;

$newpass = @$_POST['newpass'];
$newpassval = @$_POST['newpassval'];

$response = [];
$errors = 0;

if ($newpass == null) {
    $errors = 1;
    $response['msg'] .= 'Please enter your new password<br />';
}
if ($newpassval == null) {
    $errors = 1;
    $response['msg'] .= 'Please re-enter your new password<br />';
}
if ($newpass !== $newpassval) {
    $errors = 1;
    $response['msg'] .= 'Passwords do not match<br />';
}
if($errors === 1) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
} else {
    $change = new Change;
    $changestatus = $change->changePassword($_SESSION['UserID'], $newpass);

    if($changestatus === 0) {
        $response['status'] = 'error';
        $response['msg'] = 'Error: The system was unable to process your request. Please try again later.';
        echo json_encode($response);
        return;
    }
    if($changestatus === 1) {
        $response['status'] = 'success';
        $response['msg'] = 'Your password was updated successfully.';
        echo json_encode($response);
        return;
    }

    $response['status'] = 'error';
    $response['msg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
}