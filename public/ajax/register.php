<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\Members\Access;

$username = @$_POST['username'];
$pass = @$_POST['password'];
$email = rawurldecode(@$_POST['email']);

$response = array();

$error = 0;

if($username === null) {
    $response['statusmsg'] .= 'Please select a username.<br />';
    $error = 1;
}

if($pass === null) {
    $response['statusmsg'] .= 'Please enter a password.<br />';
    $error = 1;
}

if($email === null) {
    $response['statusmsg'] .= 'Please enter a email address.<br />';
    $error = 1;
} elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $response['statusmsg'] .= 'Please enter a valid email address.<br />';
    $error = 1;
}

if($error === 1) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
} else {
    $reg = new Access();
    $regstatus = $reg->register($username, $pass, $email);

    if($regstatus === 0) {
        $response['status'] = 'error';
        $response['statusmsg'] = 'Please choose a different username.';
        echo json_encode($response);
        return;
    }
    if($regstatus === 2) {
        $response['status'] = 'error';
        $response['statusmsg'] = 'There was an error. Please try again later.<br />If this error presists please inform the site administrator.';
        echo json_encode($response);
        return;
    }
    if($regstatus === 1) {
        $response['status'] = 'success';
        $response['statusmsg'] = 'Congratulations, you have successfully registerd.<br />Please login.';
        echo json_encode($response);
        return;
    }

    $response['status'] = 'error';
    $response['statusmsg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
}


