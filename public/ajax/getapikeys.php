<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';

use Blacklist\API\Manage;

$response = ['msg' => null, 'status' => null];

$getkeys = new Manage;
$keys = $getkeys->getpilotkeys($_SESSION['UserID']);

if($keys === 2) {
    $response['status'] = 'success';
    $response['msg'] = 'No API Keys found Please add your API Keys.';
    echo json_encode($response);
    return;
} elseif($keys === 0) {
    $response['status'] = 'error';
    $response['msg'] = 'There was an error. Please inform the site administrator.';
    echo json_encode($response);
    return;
} else {
    $numkeys = count($keys) - 1;

    for($x = 0; $x <= $numkeys; $x++) {

        if($keys[$x]['ExpireDate'] > date('Y-m-d H:i:s')) {
            $expired = 'NO';
        } else {
            $expired = 'YES';
        }

        $response['msg'] .= '<dl class="dl-horizontal">';
        $response['msg'] .= '<dt>Key ID</dt>';
        $response['msg'] .= '<dd name="keyid' . $x . '">' . $keys[$x]['KeyID'] . '</dd>';
        $response['msg'] .= '<dt>Verification Code</dt>';
        $response['msg'] .= '<dd name="vcode' . $x . '">' . $keys[$x]['VCode'] . '</dd>';
        $response['msg'] .= '<dt>Key Type</dt>';
        $response['msg'] .= '<dd name="keytype' . $x . '">' . $keys[$x]['KeyType'] . '</dd>';
        $response['msg'] .= '<dt>Expired</dt>';
        $response['msg'] .= '<dd>' . $expired . '</dd>';
        $response['msg'] .= '<dt><button class="text-center btn btn-primary center" onclick="deletepilotkeys(event, \'' . $keys[$x]['KeyID'] . '\')">Delete Key</button></dt>';
        $response['msg'] .= '</dl>';
        $response['msg'] .= '<br />';
        $response['msg'] .= '<br />';
        $response['msg'] .= '<br />';
    }
    $response['status'] = 'success';
    echo json_encode($response);
    return;
}
