#!/usr/bin/env php
<?php
require_once '/usr/share/nginx/testing-blacklist/private/settings.inc';
require_once $AA['db_info'];
require_once $AA['autoload'];
require_once $AA['c_api'];

$errorlog = $AA['error_log'];
$dbuser = DB_USER;
$dbhost = DB_HOST_WRITE;
$dbblacklist = DB_BLACKLIST;
$dbpass = DB_PASS;

use Pheal\Pheal;
use Pheal\Core\Config;

$phealcache = $AA['private'] . '/phealcache/';

Config::getInstance()->cache = new \Pheal\Cache\FileStorage($phealcache);
Config::getInstance()->access = new \Pheal\Access\StaticCheck();


$db = new PDO("mysql:host=" . $dbhost . ";dbname=" . $dbblacklist, $dbuser, $dbpass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

# Get all API Keys to run through
$stmt = $db->prepare('SELECT MemberID, CharacterID, KeyID, VCode FROM APIKeys WHERE KeyType = :account GROUP BY CharacterID');
$stmt->execute(array(':account' => 'Account'));
$characters = $stmt->fetchAll(PDO::FETCH_ASSOC);

# Loop through Keys and get values
$charcount = count($characters) - 1;


for ($x = 0; $x <= $charcount; $x++) {
    try {
        $keyid = $characters[$x]['KeyID'];
        $vCode = $characters[$x]['VCode'];
        $charID = $characters[$x]['CharacterID'];

        $accesstest = new EVEApi;
        $hasaccess = $accesstest->checkmask($keyid, $vCode, 'Character', 'AccountBalance');

        if($hasaccess) {

            $pheal = new Pheal($keyid, $vCode, 'char');

            $response = $pheal->AccountBalance(array('characterID' => $charID));

            $balance = $response->accounts[0]->balance;
            $accountID = $response->accounts[0]->accountID;
            $accountkey = $response->accounts[0]->accountKey;
            $times = date('Y-m-d H:i:s');

            $stmt = $db->prepare('INSERT INTO PlayerBalance(MemberID, CharacterID, AccountID, AccountKey, Balance, Times)' .
                ' VALUES(:memberid, :charid, :acctid, :acctkey, :bal, :times)');
            $stmt->execute(array(':memberid' => $characters[$x]['MemberID'], ':charid' => $characters[$x]['CharacterID'], ':acctid' => $accountID, ':acctkey' => $accountkey, ':bal' => $balance, ':times' => $times));
        }

    } catch(\Pheal\Exceptions\PhealException $ex) {
        error_log(date('Y-m-d H:i:s') . ' -> PHEAL ERROR: ' .$ex->getMessage() . "\n", 3, $AA['error_log']);
    } catch(PDOException $ex) {
        error_log(date('Y-m-d H:i:s') . ' -> PDO ERROR: ' .$ex->getMessage() . "\n", 3, $AA['error_log']);
    }

}

