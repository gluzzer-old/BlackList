#!/usr/bin/env php
<?php
require_once 'settings.inc';
require_once $dir . '/vendor/autoload.php';

use Blacklist\API\Calls\Server;

$req = new Server;
$req->getServerStatus();
