<?php
namespace Blacklist\API\Calls;

require __DIR__ . '/../../dbinfo.inc';


use PDO;
use PDOException;
use Pheal\Exceptions\PhealException;
use Pheal\Pheal;

class Server
{
    private $db = DSN;
    protected $dsn = DSN;
    public $dbuser = DBUSER;
    public $dbpass = DBPASS;

    public function __construct()
    {
        try {
            $this->db = new PDO($this->dsn, $this->dbuser, $this->dbpass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
        }

        $this->pheal = new Pheal();
    }

    public function getServerStatus()
    {
        try {
            $response = $this->pheal->serverScope->ServerStatus();

            $online = $response->serverOpen;
            $playercount = $response->onlinePlayers;
            $time = date('Y-m-d H:i:s');

            if ($online === 'True') {
                $online = 1;
            } else {
                $online = 0;
            }
        } catch (PhealException $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        }

        try {
            $stmt = $this->db->prepare('INSERT INTO ServerStatus(ServerStatus, OnlinePlayers, Times) VALUES(:serverstatus, :onlineplayers, :times)');
            $stmt->execute([':serverstatus' => $online, ':onlineplayers' => $playercount, ':times' => $time]);
            $this->db = null;
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        }

        return 1;

    }
}