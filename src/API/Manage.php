<?php
namespace Blacklist\API;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/dbinfo.inc';

use PDO;
use PDOException;
use Exception;
use Pheal\Pheal;
use Pheal\Cache\PdoStorage;
use Pheal\Core\Config;
use Pheal\Access\StaticCheck;
use Pheal\Exceptions\PhealException;



class Manage
{
    private $db;
    protected $dsn = DSN;
    public $dbuser = DBUSER;
    public $dbpass = DBPASS;

    public function __construct() {

        try {
            $this->db = new PDO($this->dsn, $this->dbuser, $this->dbpass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            Config::getInstance()->cache = new PdoStorage($this->dsn, $this->dbuser, $this->dbpass);
            Config::getInstance()->access = new StaticCheck();
        } catch (PhealException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
        }

    }

    public function addApi($UserID, $keyID, $vCode) {
        $pheal = new Pheal($keyID, $vCode);

        try {
            $response = $pheal->Characters();
            $keyinfo = $pheal->APIKeyInfo();
            $expiredate = $keyinfo->key->expires;
            $keytype = $keyinfo->key->type;
            $chars = count($response);
            $date = date('Y-m-d H:i:s');

            if ($expiredate != null && $date >= $expiredate) {
                return 0;
            }
            if ($expiredate == null) {
                $expiredate = '2100-12-31 00:00:00';
            }

            if ($keytype === 'Corporation') {

                $name = $response->characters[0]['name'];
                $characterID = $response->characters[0]['characterID'];
                $corporationID = $response->characters[0]['corporationID'];
                $corporationName = $response->characters[0]['corporationName'];
                $allianceID = $response->characters[0]['allianceID'];
                $allianceName = $response->characters[0]['allianceName'];
                $factionID = $response->characters[0]['factionID'];
                $factionName = $response->characters[0]['factionName'];

                $stmt = $this->db->prepare('INSERT INTO Corporations(UserID, CharacterName, CharacterID, CorpID, CorpName, AllianceID, AllianceName, FactionID, FactionName)' .
                    ' VALUES(:memberid, :charname, :charid, :corpid, :corpname, :allid, :allname, :factid, :factname)');
                $stmt->execute([':memberid' => $UserID, ':charname' => $name, ':charid' => $characterID, ':corpid' => $corporationID, ':corpname' => $corporationName,
                    ':allid' => $allianceID, ':allname' => $allianceName, ':factid' => $factionID, ':factname' => $factionName]);
                $stmt = $this->db->prepare('INSERT INTO APIKeys(UserID, CharacterID, KeyID, VCode, ExpireDate, KeyType)' .
                    ' VALUES(:memberid, :charid, :keyid, :vcode, :expiredate, :keytype)');
                $stmt->execute([':memberid' => $UserID, ':charid' => $characterID, ':keyid' => $keyID, ':vcode' => $vCode, ':expiredate' => $expiredate, ':keytype' => $keytype]);

            } else {

                for ($x = 0; $x <= $chars; $x++) {
                    $name = $response->characters[$x]['name'];
                    $characterID = $response->characters[$x]['characterID'];
                    $corporationID = $response->characters[$x]['corporationID'];
                    $corporationName = $response->characters[$x]['corporationName'];
                    $allianceID = $response->characters[$x]['allianceID'];
                    $allianceName = $response->characters[$x]['allianceName'];
                    $factionID = $response->characters[$x]['factionID'];
                    $factionName = $response->characters[$x]['factionName'];

                    $stmt = $this->db->prepare('INSERT INTO Characters(UserID, CharacterName, CharacterID, CorporationID, CorporationName, AllianceID, AllianceName, FactionID, FactionName)' .
                        ' VALUES(:memberid, :charname, :charid, :corpid, :corpname, :allid, :allname, :factid, :factname)');
                    $stmt->execute([':memberid' => $UserID, ':charname' => $name, ':charid' => $characterID, ':corpid' => $corporationID, ':corpname' => $corporationName,
                        ':allid' => $allianceID, ':allname' => $allianceName, ':factid' => $factionID, ':factname' => $factionName]);
                    $stmt = $this->db->prepare('INSERT INTO APIKeys(UserID, CharacterID, KeyID, VCode, ExpireDate, KeyType)' .
                        ' VALUES(:memberid, :charid, :keyid, :vcode, :expiredate, :keytype)');
                    $stmt->execute([':memberid' => $UserID, ':charid' => $characterID, ':keyid' => $keyID, ':vcode' => $vCode, ':expiredate' => $expiredate, ':keytype' => $keytype]);
                }
            }

        }  catch (PhealException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        } catch (Exception $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        }

        $this->db = null;
        return 1;

    }

    public function deletepilotkeys($UserID, $KeyID) {
        try {
            $stmt = $this->db->prepare('SELECT CharacterID, KeyType FROM APIKeys WHERE KeyID = :keyid');
            $stmt->execute([':keyid' => $KeyID]);
            $keys = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($keys[0]['KeyType'] === 'Corporation') {

                $stmt = $this->db->prepare('DELETE FROM Corporations WHERE UserID = :memberid AND CharacterID = :charid');
                $stmt->execute([':memberid' => $UserID, ':charid' => $keys[0]['CharacterID']]);

            } else {

                $charcount = count($keys) - 1;

                $stmt = $this->db->prepare('DELETE FROM Characters WHERE UserID = :memberid AND CharacterID = :charid');

                for ($x = 0; $x <= $charcount; $x++) {
                    $stmt->execute([':memberid' => $UserID, ':charid' => $keys[$x]['CharacterID']]);
                }
            }

            $stmt = $this->db->prepare('DELETE FROM APIKeys WHERE UserID = :memberid AND KeyID = :keyid');
            $stmt->execute([':memberid' => $UserID, ':keyid' => $KeyID]);

            return 1;

        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        }
    }

    public function getpilotkeys($UserID) {
        try {
            $stmt = $this->db->prepare('SELECT KeyID, VCode, ExpireDate, KeyType FROM APIKeys WHERE UserID = :memberid GROUP BY KeyID, VCode');
            $stmt->execute([':memberid' => $UserID]);
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if($results == null) {
                return 2;
            }
            return $results;
        } catch(PDOException $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        } catch(Exception $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        }
    }

    public function checkmask($KeyID, $vCode, $AccessType, $AccessName) {
        try {
            $stmt = $this->db->prepare('SELECT AccessMask FROM AccessMasks WHERE AccessType = :accesstype AND AccessName = :accessname');
            $stmt->execute([':accesstype' => $AccessType, ':accessname' => $AccessName]);
            $accessmask = $stmt->fetch(PDO::FETCH_ASSOC);

            $pheal = new Pheal($KeyID, $vCode, 'account');
            $keyinfo = $pheal->APIKeyInfo();
            $keymask = $keyinfo->key->accessMask;

            if($accessmask & $keymask) {
                return 1;
            } else {
                return 0;
            }


        } catch (PhealException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            return 0;
        }
    }
}