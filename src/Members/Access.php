<?php
namespace Blacklist\Members;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/dbinfo.inc';

use PDO;
use PDOException;

class Access
{
    private $db;
    public $dbhost = DBHOST;
    public $dbport = DBPORT;
    public $dbuser = DBUSER;
    public $dbpass = DBPASS;
    public $dbname = DBBL;

    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:dbname=" . $this->dbname . ";host=" . $this->dbhost . ";port=" . $this->dbport, $this->dbuser, $this->dbpass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
        }
    }

    public function register($username, $password, $email)
    {
        # Grabbing Date & Time for records
        $regdate = date('Y-m-d H:i:s');

        # Grab the username if it exists
        try {
            $stmt = $this->db->prepare('SELECT UserName from Users WHERE UserName=:username');
            $stmt->execute([':username' => $username]);
            $results = $stmt->fetch(PDO::FETCH_NUM);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
            $this->db = null;
            return 2;
        }
        # Make sure that the username isn't taken else register user.
        if ($results[0] === $username) {
            $this->db = null;
            return 0;
        } else {
            try {
                # Encrypt the password
                $password = password_hash($password, PASSWORD_DEFAULT);
                # Enter User info into the database
                $stmt = $this->db->prepare('INSERT INTO Users(UserName, UserPass, UserEmail, RegDate) ' .
                                           'VALUES(:username, :userpass, :useremail, :regdate)');
                $stmt->execute([':username' => $username, ':userpass' => $password, ':useremail' => $email, ':regdate' => $regdate]);
                $this->db = null;
                return 1;
            } catch (PDOException $ex) {
                error_log($ex->getMessage().$ex->getTrace());
                $this->db = null;
                return 2;
            }
        }
    }

    public function login($username, $password)
    {
        # Get time of login
        $logintime = date('Y-m-d H:i:s');
        # Get client IP - May not be reliable
        $loginip = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP);

        # Get info for checking login and for setting variables
        try {
            $stmt = $this->db->prepare('SELECT * FROM Users WHERE UserName = :username');
            $stmt->execute([':username' => $username]);
            $results = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $ex) {
            $this->db = null;
            error_log($ex->getMessage().$ex->getTrace());
            return 2;
        }

        if ($results === null || $results['AccountStatus'] === 0) {
            try {
                $stmt = $this->db->prepare('UPDATE Users SET LastFailedIP = :lastfailedip, LastFailed = :lastfailed, FailCount = :failcount WHERE UserName = :username');
                $stmt->execute([':lastfailedip' => $loginip, ':lastfailed' => $logintime, ':failcount' => $results['FailCount'] + 1, ':username' => $username]);
                $this->db = null;
                return 0;
            } catch (PDOException $ex) {
                $this->db = null;
                error_log($ex->getMessage() . $ex->getTrace());
                return 2;
            }
        } else {
            if (password_verify($password, $results['UserPass'])) {
                try {
                    $stmt = $this->db->prepare('UPDATE Users SET LastLogin = :lastlogin, LastIP = :lastip, FailCount = :failcount WHERE UserName = :username');
                    $stmt->execute([':lastlogin' => $logintime, ':lastip' => $loginip, ':failcount' => 0, ':username' => $username]);
                } catch (PDOException $ex) {
                    $this->db = null;
                    error_log($ex->getMessage().$ex->getTrace());
                    return 2;
                }

                $_SESSION['login'] = 1;
                $_SESSION['UserID'] = $results['UserID'];
                $_SESSION['UserName'] = $username;

                if ($results['FailCount'] > 0) {
                    $_SESSION['FailCount'] = $results['FailCount'];
                    $_SESSION['LastFailIP'] = $results['LastFailIP'];

                    try {
                        $stmt = $this->db->prepare('UPDATE Users SET LastFailed = 0, LastFailedIP = 0 WHERE UserName = :username');
                        $stmt->execute([':username' => $username]);
                    } catch (PDOException $ex) {
                        $this->db = null;
                        error_log($ex->getMessage().$ex->getTrace());
                        return 2;
                    }
                }
                $this->db = null;
                return 1;
            } else {
                if ($results['FailCount'] < 5) {
                    try {
                        $stmt = $this->db->prepare('UPDATE Users SET LastFailedIP = :lastfailedip, LastFailed = :lastfailed, FailCount = :failcount WHERE UserName = :username');
                        $stmt->execute([':lastfailedip' => $loginip, ':lastfailed' => $logintime, ':failcount' => $results['FailCount'] + 1, ':username' => $username]);
                        $this->db = null;
                        return 0;
                    } catch (PDOException $ex) {
                        $this->db = null;
                        error_log($ex->getMessage() . $ex->getTrace());
                        return 2;
                    }
                } else {
                    try {
                        $stmt = $this->db->prepare('UPDATE Users SET LastFailedIP = :lastfailedip, LastFailed = :lastfailed, FailCount = :failcount, AccountStatus = :accountstatus WHERE UserName = :username');
                        $stmt->execute([':lastfailedip' => $loginip, ':lastfailed' => $logintime, ':failcount' => $results['FailCount'] + 1, ':username' => $username, ':accountstatus' => 0]);
                        $this->db = null;
                        return 0;
                    } catch (PDOException $ex) {
                        $this->db = null;
                        error_log($ex->getMessage() . $ex->getTrace());
                        return 2;
                    }
                }
            }
        }

    }

    public function logout()
    {
        $_SESSION = null;

        if(ini_get('session.use_cookie')) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params['path'], $params['domain'], $params['secure'],
                $params['httponly']);
        }

        session_destroy();

        return 1;
    }
}