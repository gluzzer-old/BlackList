<?php
namespace Blacklist\Members;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/dbinfo.inc';

use PDO;
use PDOException;

class Change
{
    private $db;
    protected $dsn = DSN;
    public $dbuser = DBUSER;
    public $dbpass = DBPASS;

    public function __construct() {

        try {
            $this->db = new PDO($this->dsn, $this->dbuser, $this->dbpass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTrace());
        }

    }

    public function changePassword($userid, $newpass)
    {
        $pass = password_hash($newpass, PASSWORD_DEFAULT);
        try {
            $stmt = $this->db->prepare('UPDATE Users SET UserPass = :userpass WHERE UserID = :userid ');
            $stmt->execute([':userpass' => $pass, ':userid' => $userid]);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        }

        return 1;
    }

    public function changeEmail($userid, $email)
    {
        try {
            $stmt = $this->db->prepare('UPDATE Users SET UserEmail = :email WHERE UserID = :userid');
            $stmt->execute([':email' => $email, ':userid' => $userid]);
        } catch (PDOException $ex) {
            error_log($ex->getMessage().$ex->getTraceAsString());
            return 0;
        }

        return 1;
    }

}